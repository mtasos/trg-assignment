export interface Marker {
  id?: string;
  lat: number;
  lng: number;
  name: string;
}
