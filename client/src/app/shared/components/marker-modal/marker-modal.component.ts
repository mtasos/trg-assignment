import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { Marker } from 'src/app/models/Marker.model';

@Component({
  selector: 'marker-modal',
  templateUrl: './marker-modal.component.html',
  styleUrls: ['./marker-modal.component.scss'],
})
export class MarkerModalComponent implements OnInit {
  public title: 'Add New Mark' | 'Edit Mark' = 'Add New Mark';

  public form: FormGroup = this.fb.group({
    id: null,
    lat: [null, [Validators.required]],
    lng: [null, [Validators.required]],
    name: [null, [Validators.required]],
  });

  constructor(
    private fb: FormBuilder,
    public dialogRef: MatDialogRef<MarkerModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Marker
  ) {}

  public ngOnInit(): void {
    if (Object.keys(this.data).length !== 0) {
      const { id, lat, lng, name } = this.data;
      this.form.get('id').setValue(id);
      this.form.get('lat').setValue(lat);
      this.form.get('lng').setValue(lng);
      this.form.get('name').setValue(name);

      this.title = 'Edit Mark';
    }
  }

  public submit() {
    this.markAsDirty(this.form);

    if (this.form.invalid) {
      return;
    }

    const markerdata = this.form.getRawValue();
    this.dialogRef.close(markerdata);
  }

  public cancel(): void {
    this.dialogRef.close();
  }

  private markAsDirty(group: FormGroup): void {
    group.markAsDirty();
    for (const i in group.controls) {
      group.controls[i].markAsDirty();
    }
  }
}
