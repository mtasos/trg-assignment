import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { MatSidenavModule } from '@angular/material/sidenav';
import { BrowserModule } from '@angular/platform-browser';

import { AgmCoreModule } from '@agm/core';
import { AgmMarkerClustererModule } from '@agm/markerclusterer';
import { environment } from 'src/environments/environment';
import { MapComponent } from './map.component';

@NgModule({
  providers: [],
  imports: [
    CommonModule,
    BrowserModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleMapsApi,
    }),
    AgmMarkerClustererModule,
    MatSidenavModule,
    MatDividerModule,
    MatButtonModule,
  ],
  declarations: [MapComponent],
  exports: [MapComponent],
})
export class MapModule {}
