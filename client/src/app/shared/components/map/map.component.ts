import { ChangeDetectionStrategy, Component, Input, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

import { Marker } from 'src/app/models/Marker.model';
import { aTonOfMarkes } from './a-ton-of-markes';

@Component({
  selector: 'dashboard-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MapComponent {
  @Input() public markers: Marker[];
  @ViewChild('drawer') public sidenav: MatSidenav;

  public DEFAULT_ZOOM = 5;
  public DEFAULT_LAT = 35.1264;
  public DEFAULT_LNG = 33.4299;

  public selectedMarker: Marker;
  public loading = false;
  public isNukeBtnDisabled = false;

  public setSelectedMarker(marker: Marker): void {
    this.selectedMarker = marker;
    this.sidenav.toggle();
  }

  public addaTonOfMarkers(): void {
    if (this.sidenav.opened) {
      this.sidenav.close();
    }
    this.DEFAULT_ZOOM = 1.8;
    this.markers = aTonOfMarkes();
  }
}
