import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashBoardComponent } from './pages/dashboard/dashboard.component';
import { DashboardGuard } from './pages/dashboard/guards/dashboard.guards';
import { MarkersListGuard } from './pages/markers-list/guards/markers-list.guards';
import { MarkersListComponent } from './pages/markers-list/markers-list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: 'dashboard',
    component: DashBoardComponent,
    canActivate: [DashboardGuard],
  },
  {
    path: 'markers',
    component: MarkersListComponent,
    canActivate: [MarkersListGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
