import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { Marker } from 'src/app/models/Marker.model';
import { environment } from 'src/environments/environment';
import { configuration as conf } from './config';

@Injectable()
export class MarkersService {
  private apiUrl = `${environment.dashboardApi}${conf.serverContext}`;

  constructor(private http: HttpClient) {}

  private createPath(): string {
    return `${this.apiUrl}/marker`;
  }

  public getAll(): Observable<Marker[]> {
    const url = this.createPath();
    return this.http.get<Marker[]>(url);
  }

  public create(lat: number, lng: number, name: string): Observable<Marker> {
    const url = this.createPath();
    const data = { lat, lng, name };
    return this.http.post<Marker>(url, data);
  }

  public update(
    id: string,
    lat: number,
    lng: number,
    name: string
  ): Observable<Marker> {
    const url = `${this.createPath()}/${id}`;
    const data = { id, lat, lng, name };
    return this.http.put<Marker>(url, data);
  }

  public delete(id: string): Observable<void> {
    const url = `${this.createPath()}/${id}`;
    return this.http.delete<void>(url);
  }
}
