import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { Marker } from 'src/app/models/Marker.model';
import { environment } from 'src/environments/environment';
import { configuration as conf } from './config';

@Injectable()
export class DashBoardService {
  private apiUrl = `${environment.dashboardApi}${conf.serverContext}`;

  constructor(private http: HttpClient) {}

  private createPath(): string {
    return `${this.apiUrl}/marker`;
  }

  public getAll(): Observable<Marker[]> {
    const url = this.createPath();
    return this.http.get<Marker[]>(url);
  }
}
