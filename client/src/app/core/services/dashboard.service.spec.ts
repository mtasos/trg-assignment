import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { configuration as conf } from './config';
import { DashBoardService } from './dashboard.service';

describe('Dashboard Service', () => {
  let service: DashBoardService;
  const httpMock = new HttpClient(null);
  const prefix = `${environment.dashboardApi}${conf.serverContext}`;
  let httpGet: any;

  beforeEach(() => {
    httpGet = spyOn(httpMock, 'get');
    service = new DashBoardService(httpMock);
  });

  it('should get all markers', () => {
    const expectedUrl = `${prefix}/marker`;
    service.getAll();
    expect(httpGet).toHaveBeenCalledWith(expectedUrl);
  });
});
