import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { configuration as conf } from './config';
import { MarkersService } from './markers.service';

describe('Markers Service', () => {
  let service: MarkersService;
  const httpMock = new HttpClient(null);
  const prefix = `${environment.dashboardApi}${conf.serverContext}`;
  let httpGet: any, httpPost: any, httpPut: any, httpDelete: any;

  beforeEach(() => {
    httpGet = spyOn(httpMock, 'get');
    httpPost = spyOn(httpMock, 'post');
    httpPut = spyOn(httpMock, 'put');
    httpDelete = spyOn(httpMock, 'delete');

    service = new MarkersService(httpMock);
  });

  it('should get all markers with proper url', () => {
    const expectedUrl = `${prefix}/marker`;
    service.getAll();
    expect(httpGet).toHaveBeenCalledWith(expectedUrl);
  });

  it('should call create with proper url', () => {
    const expectedUrl = `${prefix}/marker`;

    const lat = 123;
    const lng = 123;
    const name = 'mockmarker';
    const payload = { lat, lng, name };

    service.create(lat, lng, name);

    expect(httpPost).toHaveBeenCalledWith(expectedUrl, payload);
  });

  it('should call update with proper url', () => {
    const expectedUrl = `${prefix}/marker/1`;

    const id = '1';
    const lat = 123;
    const lng = 123;
    const name = 'mockmarker';
    const payload = { id, lat, lng, name };

    service.update(id, lat, lng, name);

    expect(httpPut).toHaveBeenCalledWith(expectedUrl, payload);
  });

  it('should call delete with proper url', () => {
    const expectedUrl = `${prefix}/marker/1`;

    service.delete('1');
    expect(httpDelete).toHaveBeenCalledWith(expectedUrl);
  });
});
