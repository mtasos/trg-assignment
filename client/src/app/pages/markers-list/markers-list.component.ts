import { AfterViewInit, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';

import { Marker } from 'src/app/models/Marker.model';
import {
  MarkerModalComponent
} from 'src/app/shared/components/marker-modal/marker-modal.component';
import { deleteMarker, updateMarker } from '../markers-list/store/markers-list.actions';
import { addMarker } from './store/markers-list.actions';
import { MarkersState } from './store/markers-list.reducers';
import { markersSelector } from './store/markers-list.selectors';

@Component({
  selector: 'markers-list',
  templateUrl: './markers-list.component.html',
  styleUrls: ['./markers-list.component.scss'],
})
export class MarkersListComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  public dataSource = new MatTableDataSource([]);
  public displayedColumns: string[] = [
    'name',
    'lat',
    'lng',
    'editAction',
    'deleteAction',
  ];

  private subscription = new Subscription();

  constructor(private store: Store<MarkersState>, public dialog: MatDialog) {}

  public ngOnInit(): void {
    this.subscription.add(
      this.store.select(markersSelector).subscribe((markers: Marker[]) => {
        this.dataSource.data = markers;
      })
    );
  }

  public ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  public filterTableData(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public createOrUpdateMarker(marker?: Marker): void {
    const data = marker
      ? {
          id: marker.id,
          lat: marker.lat,
          lng: marker.lng,
          name: marker.name,
        }
      : {};

    const dialogRef = this.dialog.open(MarkerModalComponent, {
      width: '250px',
      data,
    });

    dialogRef.afterClosed().subscribe((result: Marker) => {
      if (!result) {
        return;
      }
      const marker: Marker = {
        id: result.id || null,
        lat: Number(result.lat),
        lng: Number(result.lng),
        name: result.name,
      };

      if (result.id) {
        this.updateMarker(marker);
      } else {
        this.addNewMarker(marker);
      }
    });
  }

  public deleteMarker(marker: Marker): void {
    this.store.dispatch(deleteMarker({ id: marker.id }));
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  private addNewMarker(marker: Marker): void {
    const { lat, lng, name } = marker;
    this.store.dispatch(addMarker({ lat, lng, name }));
  }

  private updateMarker(marker: Marker): void {
    const { id, lat, lng, name } = marker;
    this.store.dispatch(updateMarker({ id, lat, lng, name }));
  }
}
