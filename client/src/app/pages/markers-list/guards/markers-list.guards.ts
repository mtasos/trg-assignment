import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, first, switchMap, tap } from 'rxjs/operators';

import { Marker } from 'src/app/models/Marker.model';
import { getMarkers } from '../store/markers-list.actions';
import { MarkersState } from '../store/markers-list.reducers';
import { markersSelector } from '../store/markers-list.selectors';

@Injectable()
export class MarkersListGuard implements CanActivate {
  constructor(private store: Store<MarkersState>) {}

  public canActivate(): Observable<boolean> {
    return this.markersListGuardFunction().pipe(
      first((markers: Marker[]) => !!markers),
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  private markersListGuardFunction(): Observable<Marker[]> {
    return this.store.pipe(
      select(markersSelector),
      tap(() => {
        this.store.dispatch(getMarkers());
      })
    );
  }
}
