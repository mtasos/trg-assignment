import { TestBed } from '@angular/core/testing';

import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold } from 'jasmine-marbles';

import { getMarkers } from '../store/markers-list.actions';
import { MarkersState } from '../store/markers-list.reducers';
import { MarkersListGuard } from './markers-list.guards';

describe('MarkersListGuard', () => {
  let guard: MarkersListGuard;
  const initialState = { Markers: { markers: [] } } as any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MarkersListGuard, provideMockStore({ initialState })],
    });

    guard = TestBed.inject(MarkersListGuard);
  });

  it('should navigate to markers list', () => {
    const store: MockStore<MarkersState> = TestBed.inject<any>(Store);
    const dispatchSpy = spyOn(store, 'dispatch');

    const expected = cold('(a|)', { a: true });
    const expectedAction = getMarkers();
    store.setState({ Markers: { markers: [{ mock: true }] } } as any);

    expect(guard.canActivate()).toBeObservable(expected);
    expect(dispatchSpy).toHaveBeenCalledWith(expectedAction);
  });
});
