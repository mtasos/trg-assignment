import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { MarkersService } from 'src/app/core/services/markers.service';
import { MarkerModalModule } from 'src/app/shared/components/marker-modal/marker-modal.module';
import { MarkersListGuard } from './guards/markers-list.guards';
import { MarkersListComponent } from './markers-list.component';
import { MarkersEffects } from './store/markers-list.effects';
import { markersReducer } from './store/markers-list.reducers';
import { markersFeatureName } from './store/store.config';

@NgModule({
  providers: [MarkersListGuard, MarkersService],
  imports: [
    CommonModule,
    MatButtonModule,
    MatToolbarModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
    MatMenuModule,
    MatIconModule,
    RouterModule,
    MarkerModalModule,
    StoreModule.forFeature(markersFeatureName, markersReducer),
    EffectsModule.forFeature([MarkersEffects]),
  ],
  declarations: [MarkersListComponent],
  exports: [MarkersListComponent],
})
export class MarkersListModule {}
