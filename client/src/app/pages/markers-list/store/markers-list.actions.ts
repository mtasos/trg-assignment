import { createAction, props } from '@ngrx/store';

import { Marker } from 'src/app/models/Marker.model';
import { markersFeatureName as featureName } from './store.config';

export const addMarker = createAction(
  `[${featureName}] Create Marker`,
  props<{ lat: number; lng: number; name: string }>()
);

export const addMarkerSuccess = createAction(
  `[${featureName}] Create Marker Success`,
  props<{ lat: number; lng: number; name: string }>()
);

export const addMarkerFail = createAction(
  `[${featureName}] Create Marker Fail`,
  props<{ error: any }>()
);

export const updateMarker = createAction(
  `[${featureName}] Update Marker`,
  props<{ id: string; lat: number; lng: number; name: string }>()
);

export const updateMarkerSuccess = createAction(
  `[${featureName}] Update Marker Success`,
  props<{ id: string; lat: number; lng: number; name: string }>()
);

export const updateMarkerFail = createAction(
  `[${featureName}] Update Marker Fail`,
  props<{ error: any }>()
);

export const getMarkers = createAction(`[${featureName}] Get Markers`);

export const getMarkersSuccess = createAction(
  `[${featureName}] Get Markers Success`,
  props<{ response: Marker[] }>()
);

export const getMarkersFail = createAction(
  `[${featureName}] Get Markers Fail`,
  props<{ error: any }>()
);

export const deleteMarker = createAction(
  `[${featureName}] Delete Marker`,
  props<{ id: string }>()
);

export const deleteMarkerSuccess = createAction(
  `[${featureName}] Delete Marker Success`,
  props<{ id: string }>()
);

export const deleteMarkerFail = createAction(
  `[${featureName}] Delete Marker Fail`,
  props<{ error: any }>()
);
