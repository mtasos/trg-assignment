import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, mergeMap, of, switchMap } from 'rxjs';

import { MarkersService } from 'src/app/core/services/markers.service';
import { Marker } from 'src/app/models/Marker.model';
import * as MarkersActions from './markers-list.actions';

@Injectable()
export class MarkersEffects {
  public loadMarkers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MarkersActions.getMarkers),
      switchMap(() =>
        this.markerService.getAll().pipe(
          map((response) => MarkersActions.getMarkersSuccess({ response })),
          catchError((error) => of(MarkersActions.getMarkersFail(error)))
        )
      )
    )
  );

  public createMarker$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MarkersActions.addMarker),
      switchMap((action) =>
        this.markerService.create(action.lat, action.lng, action.name).pipe(
          map((response: Marker) =>
            MarkersActions.addMarkerSuccess({
              lat: response.lat,
              lng: response.lng,
              name: response.name,
            })
          ),
          catchError((error) => of(MarkersActions.addMarkerFail(error)))
        )
      )
    )
  );

  public updateMarker$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MarkersActions.updateMarker),
      switchMap((action) =>
        this.markerService
          .update(action.id, action.lat, action.lng, action.name)
          .pipe(
            map((response: Marker) =>
              MarkersActions.updateMarkerSuccess({
                id: response.id,
                lat: response.lat,
                lng: response.lng,
                name: response.name,
              })
            ),
            catchError((error) => of(MarkersActions.updateMarkerFail(error)))
          )
      )
    )
  );

  public deleteMarker$ = createEffect(() =>
    this.actions$.pipe(
      ofType(MarkersActions.deleteMarker),
      switchMap((action) =>
        this.markerService.delete(action.id).pipe(
          map(() => MarkersActions.deleteMarkerSuccess({ id: action.id })),
          catchError((error) => of(MarkersActions.deleteMarkerFail(error)))
        )
      )
    )
  );

  public updateMarkersList$ = createEffect(() =>
    this.actions$.pipe(
      ofType(
        MarkersActions.updateMarkerSuccess,
        MarkersActions.deleteMarkerSuccess
      ),
      mergeMap(() => [MarkersActions.getMarkers()])
    )
  );

  constructor(
    private actions$: Actions,
    private markerService: MarkersService
  ) {}
}
