import { createFeatureSelector, createSelector } from '@ngrx/store';

import { MarkersState } from './markers-list.reducers';
import { markersFeatureName } from './store.config';

export const selectDashboardState =
  createFeatureSelector<MarkersState>(markersFeatureName);

export const markersSelector = createSelector(
  selectDashboardState,
  (state: MarkersState) => state.markers
);
