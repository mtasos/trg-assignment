import { createReducer, on } from '@ngrx/store';

import { Marker } from 'src/app/models/Marker.model';
import { addMarker, getMarkersFail, getMarkersSuccess } from './markers-list.actions';

export interface MarkersState {
  markers: Marker[];
}

export const initialState: MarkersState = {
  markers: [],
};

export const markersReducer = createReducer<MarkersState>(
  initialState,

  on(addMarker, (state, { lat, lng, name }) => ({
    ...state,
    markers: [...state.markers, { lat, lng, name }],
  })),

  on(getMarkersSuccess, (state, { response }) => ({
    ...state,
    markers: response,
  })),

  on(getMarkersFail, (state) => ({ ...state, markers: [] }))
);
