import { TestBed } from '@angular/core/testing';

import { provideMockActions } from '@ngrx/effects/testing';
import { cold, hot } from 'jasmine-marbles';
import { Observable, of } from 'rxjs';

import { MarkersService } from 'src/app/core/services/markers.service';
import {
  addMarker, addMarkerSuccess, deleteMarker, deleteMarkerSuccess, getMarkers, getMarkersSuccess,
  updateMarker, updateMarkerSuccess
} from './markers-list.actions';
import { MarkersEffects } from './markers-list.effects';

describe('MarkersEffects', () => {
  let effects: MarkersEffects;
  let actions: Observable<any>;
  let markerServiceSpy: jasmine.SpyObj<MarkersService>;

  beforeEach(() => {
    markerServiceSpy = jasmine.createSpyObj('markerServiceSpy', [
      'getAll',
      'create',
      'update',
      'delete',
    ]);

    TestBed.configureTestingModule({
      providers: [
        MarkersEffects,
        provideMockActions(() => actions),
        {
          provide: MarkersService,
          useValue: markerServiceSpy,
        },
      ],
    });

    markerServiceSpy = TestBed.inject<any>(MarkersService);
    effects = TestBed.inject(MarkersEffects);
  });

  it('should get all markers', () => {
    const id = '1';
    const lat = 123;
    const lng = 123;
    const name = 'mockmarker';
    const response = [{ id, lat, lng, name }];

    markerServiceSpy.getAll.and.returnValue(of(response));
    const action = getMarkers();
    const completion = getMarkersSuccess({ response });

    actions = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.loadMarkers$).toBeObservable(expected);
  });

  it('should create a marker', () => {
    const lat = 123;
    const lng = 123;
    const name = 'mockmarker';
    const response = { lat, lng, name };

    markerServiceSpy.create.and.returnValue(of(response));
    const action = addMarker(response);
    const completion = addMarkerSuccess(response);

    actions = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.createMarker$).toBeObservable(expected);
  });

  it('should update a marker', () => {
    const id = '1';
    const lat = 123;
    const lng = 123;
    const name = 'mockmarker';
    const response = { id, lat, lng, name };

    markerServiceSpy.update.and.returnValue(of(response));
    const action = updateMarker(response);
    const completion = updateMarkerSuccess(response);

    actions = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.updateMarker$).toBeObservable(expected);
  });

  it('should delete a marker', () => {
    const id = '1';
    const response = { id };

    markerServiceSpy.delete.and.returnValue(of(null));
    const action = deleteMarker(response);
    const completion = deleteMarkerSuccess(response);

    actions = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.deleteMarker$).toBeObservable(expected);
  });
});
