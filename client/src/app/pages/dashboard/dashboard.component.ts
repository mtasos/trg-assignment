import { Component, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';

import { Marker } from 'src/app/models/Marker.model';
import { DashboardState } from './store/dashboard.reducers';
import { markersSelector } from './store/dashboard.selectors';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashBoardComponent implements OnInit {
  public markers$: Observable<Marker[]>;

  constructor(private store: Store<DashboardState>) {}

  public ngOnInit(): void {
    this.markers$ = this.store.select(markersSelector);
  }
}
