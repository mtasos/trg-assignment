import { createFeatureSelector, createSelector } from '@ngrx/store';

import { DashboardState } from './dashboard.reducers';
import { dashboardFeatureName } from './store.config';

export const selectDashboardState =
  createFeatureSelector<DashboardState>(dashboardFeatureName);

export const markersSelector = createSelector(
  selectDashboardState,
  (state: DashboardState) => state.markers
);
