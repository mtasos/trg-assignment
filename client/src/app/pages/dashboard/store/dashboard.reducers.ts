import { createReducer, on } from '@ngrx/store';

import { Marker } from 'src/app/models/Marker.model';
import { getMarkersFail, getMarkersSuccess } from './dashboard.actions';

export interface DashboardState {
  markers: Marker[];
}

export const initialState: DashboardState = {
  markers: [],
};

export const dashboardReducer = createReducer<DashboardState>(
  initialState,

  on(getMarkersSuccess, (state, { response }) => ({
    ...state,
    markers: response,
  })),

  on(getMarkersFail, (state) => ({ ...state, markers: [] }))
);
