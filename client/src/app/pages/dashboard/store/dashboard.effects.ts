import { Injectable } from '@angular/core';

import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, map, of, switchMap } from 'rxjs';

import { DashBoardService } from 'src/app/core/services/dashboard.service';
import * as DashboardActions from './dashboard.actions';

@Injectable()
export class DashboardEffects {
  public loadMarkers$ = createEffect(() =>
    this.actions$.pipe(
      ofType(DashboardActions.getMarkers),
      switchMap(() =>
        this.dashboardService.getAll().pipe(
          map((response) => DashboardActions.getMarkersSuccess({ response })),
          catchError((error) => of(DashboardActions.getMarkersFail(error)))
        )
      )
    )
  );
  constructor(
    private actions$: Actions,
    private dashboardService: DashBoardService
  ) {}
}
