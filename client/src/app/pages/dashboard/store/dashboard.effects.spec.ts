import { TestBed } from '@angular/core/testing';

import { provideMockActions } from '@ngrx/effects/testing';
import { cold, hot } from 'jasmine-marbles';
import { Observable, of } from 'rxjs';

import { DashBoardService } from 'src/app/core/services/dashboard.service';
import { getMarkers, getMarkersSuccess } from './dashboard.actions';
import { DashboardEffects } from './dashboard.effects';

describe('DashboardEffects', () => {
  let effects: DashboardEffects;
  let actions: Observable<any>;
  let dashboardServiceSpy: jasmine.SpyObj<DashBoardService>;

  beforeEach(() => {
    dashboardServiceSpy = jasmine.createSpyObj('dashboardServiceSpy', [
      'getAll',
    ]);

    TestBed.configureTestingModule({
      providers: [
        DashboardEffects,
        provideMockActions(() => actions),
        {
          provide: DashBoardService,
          useValue: dashboardServiceSpy,
        },
      ],
    });

    dashboardServiceSpy = TestBed.inject<any>(DashBoardService);
    effects = TestBed.inject(DashboardEffects);
  });

  it('should get all markers', () => {
    const id = '1';
    const lat = 123;
    const lng = 123;
    const name = 'mockmarker';
    const response = [{ id, lat, lng, name }];

    dashboardServiceSpy.getAll.and.returnValue(of(response));
    const action = getMarkers();
    const completion = getMarkersSuccess({ response });

    actions = hot('--a-', { a: action });
    const expected = cold('--b', { b: completion });

    expect(effects.loadMarkers$).toBeObservable(expected);
  });
});
