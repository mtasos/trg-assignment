import { createAction, props } from '@ngrx/store';

import { Marker } from 'src/app/models/Marker.model';
import { dashboardFeatureName as featureName } from './store.config';

export const getMarkers = createAction(`[${featureName}] Get Markers`);

export const getMarkersSuccess = createAction(
  `[${featureName}] Get Markers Success`,
  props<{ response: Marker[] }>()
);

export const getMarkersFail = createAction(
  `[${featureName}] Get Markers Fail`,
  props<{ error: any }>()
);
