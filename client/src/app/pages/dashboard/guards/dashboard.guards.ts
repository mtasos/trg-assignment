import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

import { select, Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, first, switchMap, tap } from 'rxjs/operators';

import { Marker } from 'src/app/models/Marker.model';
import { getMarkers } from '../store/dashboard.actions';
import { DashboardState } from '../store/dashboard.reducers';
import { markersSelector } from '../store/dashboard.selectors';

@Injectable()
export class DashboardGuard implements CanActivate {
  constructor(private store: Store<DashboardState>) {}

  public canActivate(): Observable<boolean> {
    return this.dashboardGuardFunction().pipe(
      first((markers: Marker[]) => !!markers),
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  private dashboardGuardFunction(): Observable<Marker[]> {
    return this.store.pipe(
      select(markersSelector),
      tap(() => {
        this.store.dispatch(getMarkers());
      })
    );
  }
}
