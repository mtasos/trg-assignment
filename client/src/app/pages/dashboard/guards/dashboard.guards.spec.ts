import { TestBed } from '@angular/core/testing';

import { Store } from '@ngrx/store';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { cold } from 'jasmine-marbles';

import { getMarkers } from '../store/dashboard.actions';
import { DashboardState } from '../store/dashboard.reducers';
import { DashboardGuard } from './dashboard.guards';

describe('DashboardGuard', () => {
  let guard: DashboardGuard;
  const initialState = { Dashboard: { markers: [] } } as any;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DashboardGuard, provideMockStore({ initialState })],
    });

    guard = TestBed.inject(DashboardGuard);
  });

  it('should allow for loading markers for the map', () => {
    const store: MockStore<DashboardState> = TestBed.inject<any>(Store);
    const dispatchSpy = spyOn(store, 'dispatch');

    const expected = cold('(a|)', { a: true });
    const expectedAction = getMarkers();
    store.setState({ Dashboard: { markers: [{ mock: true }] } } as any);

    expect(guard.canActivate()).toBeObservable(expected);
    expect(dispatchSpy).toHaveBeenCalledWith(expectedAction);
  });
});
