import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { RouterModule } from '@angular/router';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { DashBoardService } from 'src/app/core/services/dashboard.service';
import { MapModule } from 'src/app/shared/components/map/map.module';
import { DashBoardComponent } from './dashboard.component';
import { DashboardGuard } from './guards/dashboard.guards';
import { DashboardEffects } from './store/dashboard.effects';
import { dashboardReducer } from './store/dashboard.reducers';
import { dashboardFeatureName } from './store/store.config';

@NgModule({
  providers: [DashBoardService, DashboardGuard],
  imports: [
    CommonModule,
    MatButtonModule,
    MapModule,
    RouterModule,
    StoreModule.forFeature(dashboardFeatureName, dashboardReducer),
    EffectsModule.forFeature([DashboardEffects]),
  ],
  declarations: [DashBoardComponent],
  exports: [DashBoardComponent],
})
export class DashBoardModule {}
