The assignment is consisted of the client and the server

 - cd to each one separately and run `npm install` for both
 PS: `I used node v14.20.0`

 For the server create an `.env` file and copy paste the values from .example.env
 The server uses a remote hosted mongodb.

 - cd into server folder and run `npm start` to start the server
 - cd into client folder and run `npm start` to start the client

For client tests run `npm run test`