import express, { Router } from 'express';

import { deleteMarker, getMarker, saveMarker, updateMarker } from '../controllers/marker';

const router: Router = express.Router();

router.post('/marker', saveMarker);
router.put('/marker/:id', updateMarker);
router.get('/marker', getMarker);
router.delete('/marker/:id', deleteMarker);

export default router;
