require('dotenv').config();

import cors from 'cors';
import express from 'express';

import { connectDB } from './config/db';
import routes from './routes/marker.route';

const app = express();
app.use(cors());

const PORT = process.env.PORT || 5000;

connectDB();

app.use(express.json());
app.use('/api/v1', routes);

const server = app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});

process.on('unhandledRejection', (error, promise) => {
  console.log(`Logged Error: ${error}`);
  server.close(() => process.exit(1));
});
