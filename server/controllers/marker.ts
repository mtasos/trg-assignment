import { NextFunction, Request, Response } from 'express';
import httpStatus from 'http-status';

import { Marker } from '../models/markers';

export const saveMarker = async (req: Request, res: Response, next: NextFunction) => {
  const { lat, lng, name } = req.body;
  try {
    if (!lat || !lng || !name) {
      throw new Error('Marker info missing');
    }

    await Marker.create({ lat, lng, name });
    res.status(httpStatus.CREATED).json({ lat, lng, name });
  } catch (error: any) {
    return next(error);
  }
};

export const updateMarker = async (req: Request, res: Response, next: NextFunction) => {
  const { lat, lng, name } = req.body;
  const id = req.params.id;
  try {
    if (!id) {
      throw new Error('Id missing');
    }

    if (!lat || !lng || !name) {
      throw new Error('Marker info missing');
    }
    const marker = await Marker.findByIdAndUpdate({ _id: id }, { lat, lng, name });

    res.status(httpStatus.OK).json({ marker });
  } catch (error: any) {
    return next(error);
  }
};

export const getMarker = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const markers = await Marker.find();

    const mappedMarkers = markers.map(marker => ({
      id: marker._id,
      lat: marker.lat,
      lng: marker.lng,
      name: marker.name
    }));

    res.status(httpStatus.OK).json(mappedMarkers);
  } catch (error: any) {
    return next(error);
  }
};

export const deleteMarker = async (req: Request, res: Response, next: NextFunction) => {
  const id = req.params.id;
  try {
    if (!id) {
      throw new Error('Id missing');
    }
    const deletedId = await Marker.findByIdAndDelete({ _id: id });

    res.status(httpStatus.OK).json({ id: deletedId });
  } catch (error: any) {
    return next(error);
  }
};
