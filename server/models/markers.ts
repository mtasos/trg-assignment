import mongoose, { model, Schema } from 'mongoose';

const markerSchema: Schema = new Schema({
  lat: {
    type: Number
  },
  lng: {
    type: Number
  },
  name: {
    type: String
  }
});

export const Marker = model('Marker', markerSchema);
